<?php

/**
 * @file
 * Node title validation admin file.
 */

/**
 * Implements hook_form().
 */
function _email_validation_admin_form($form, &$form_state) {

  $form['email_validation_rule'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email condition'),
    '#collapsible' => FAlSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['email_validation_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Configuration'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $email_validation_rule = variable_get('email_validation_rule', array());
  $email_validation_config = variable_get('email_validation_config', array());

  $form['email_validation_rule']['blacklist_email'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($email_validation_rule['blacklist_email']) ? $email_validation_rule['blacklist_email'] : '',
    '#title' => t('Blacklist Emails'),
    '#description' => '<p>' . t("Comma separated emails to avoided while saving emaill. Ex: abc@example.com.") . '</p>' . '<p>' . t('If any of the blacklisted emails found in email ,would return validation error on user save.') . '</p>',
  );

  $form['email_validation_rule']['min_char'] = array(
    '#type' => 'textfield',
    '#title' => t("Minimum characters"),
    '#required' => TRUE,
    '#description' => t("Minimum number of characters email should contain"),
    '#size' => 12,
    '#maxlength' => 3,
    '#default_value' => isset($email_validation_rule['min_char']) ? $email_validation_rule['min_char'] : '1',
  );
  $form['email_validation_rule']['max_char'] = array(
    '#type' => 'textfield',
    '#title' => t("Maximum characters"),
    '#required' => TRUE,
    '#description' => t("Maximum number of characters email should contain"),
    '#size' => 12,
    '#maxlength' => 3,
    '#default_value' => isset($email_validation_rule['max_char']) ? $email_validation_rule['max_char'] : '60',
  );

   $form['email_validation_config']['email_label'] = array(
    '#type' => 'textfield',
    '#title' => t("Email Label"),
    '#description' => t("This value will display instead of email in the registration form"),
    '#size' => 55,
    '#maxlength' => 55,
    '#default_value' => isset($email_validation_config['email_label']) ? $email_validation_config['email_label'] : '',
  );

  $form['email_validation_config']['user_desc'] = array(
    '#type' => 'textfield',
    '#title' => t("Email description"),
    '#description' => t("This value will display as email description"),
    '#size' => 55,
    '#maxlength' => 55,
    '#default_value' => isset($email_validation_config['email_desc']) ? $email_validation_config['email_desc'] : '',
  );
  
  return system_settings_form($form);
}

/**
 * The validation function for email_config_validation_admin_form form.
 */
function _email_validation_admin_form_validate($form, &$form_state) {
  $max = $form_state['values']['email_validation_rule']['max_char'];
  // Validate field is numerical.
  if (!is_numeric($max)) {
    form_set_error('max_char', t("These value should be Numerical"));
  }

  $min = $form_state['values']['email_validation_rule']['min_char'];

  // Validate field is numerical.
  if (!is_numeric($min)) {
    form_set_error('email_validation_rule][min_char', t("These value should be Numerical"));
  }

  // Validate min is less than max value.
  if ($min > $max) {
    form_set_error('email_validation_rule][min_char', t("Minimum length should not be more than Max length"));
  }
}
